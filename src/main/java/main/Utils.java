/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.time.format.DateTimeFormatter;

/**
 * 
 * @author manel
 */
public class Utils {
    
     /***
     * Retorna un format fix i unificat pel camp data
     * Tota referencia a DateTime de l'aplicació fan servir el mateix format
     * @return 
     */
    public static DateTimeFormatter getDataFormatter() {
        String format = "dd-MM-yyyy HH:mm:ss";
        DateTimeFormatter dataFormatter = DateTimeFormatter.ofPattern(format);
        
        return dataFormatter;
    }
    
}
