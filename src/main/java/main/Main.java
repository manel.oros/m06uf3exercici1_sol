/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.logging.Level;
import java.util.logging.Logger;
import dades.entitats.PostData;
import logica.PostLogic;
import presentacio.Vista;

/**
 * @author manel
 * 
 * 
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try{

            // Establim el nivell de log del driver de mongodb
            Logger mongoLogger = Logger.getLogger( "org.mongodb.driver" );
            mongoLogger.setLevel(Level.WARNING);
         
            //Inicialitzem capa de dades
            PostData dataLayer = new PostData("exercici1");
            
            // inicialitzem capa logica
            PostLogic logic = new PostLogic(dataLayer);
            
            // inicialitzem capa presentacio
            Vista vista = new Vista(logic);

            Integer opc = 0;

            while (opc != 5)
            {
                //obtenim opció seleccionada i processem
                opc = vista.getMenuPrincipal();

                switch (opc){
                    case 1:{
                       
                        vista.menuCrearPost(); break;
                    }
                    case 2:{
                       
                        vista.menuVeure();break;
                    }
                    case 3:{
                       
                        vista.menuEliminarPost();break;
                    }
                    case 4:{
                       
                        vista.menuValorarPost();break;
                    }
                }
            }

            // tanquem connexió
            dataLayer.getMongoClient().close();
            
            //sortim sense error
            System.exit(0);

        }
        catch (Exception ex)
        {
            System.out.println(ex.toString());
            System.exit(1);
        }
    }
}
