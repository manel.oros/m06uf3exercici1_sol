/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacio;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Scanner;
import dades.entitats.Post;
import java.util.Iterator;
import java.util.List;
import logica.PostLogic;
import main.Utils;

/**
 *
 * @author manel
 */
public class Vista {
    
    PostLogic logicLayer;
    
    public Vista(PostLogic layer)
    {
        this.logicLayer = layer;
    }
    
    /***
    * Retorna la opció de menu seleccionada per l'usuari
    * @return 
    */
    public int getMenuPrincipal()
    {
        int opc = 1;
        boolean sortir = false;
        
        do
        {
            System.out.println("MENU PRINCIPAL");
            System.out.println("==== =========");
            System.out.println("1 - Crear post");
            System.out.println("2 - Veure posts");
            System.out.println("3 - Eliminar post");
            System.out.println("4 - Valorar post");
            System.out.println("5 - Sortir");
            System.out.println("");
            System.out.println("Seleccionar opció("+opc+")");
            
            try{
                Scanner in = new Scanner(System.in);
                String op = in.nextLine(); 
                opc = Integer.parseInt(op);
                if (opc<1 || opc >5) 
                    throw new Exception();
                sortir = true;
            }
            catch (Exception ex)
            {
                System.out.println("Opció incorrecta");
            };
            
        }
        while (!sortir);
        
        return opc;
    }
    
    /***
     * Crea un missatge en funció de les dades introduides per l'usuari
     * @return 
     */
    public Post menuCrearPost()
    {
        Post ret = new Post();
        
        System.out.println("NOU MISSATGE");
        System.out.println("=== ========");
        
        try
        {
            Scanner in = new Scanner(System.in);
            
            System.out.println("Autor?");
            ret.setAutor(in.nextLine());

            System.out.println("Titol?");
            ret.setTitol(in.nextLine());

            System.out.println("Contingut? (!q per finalitzar)");
            
            String str = "";
            boolean sortir = false;
            
            while (!sortir)
            {
                String linia = in.nextLine();
                if (linia.equals("!q"))
                    sortir = true;
                else
                    str += linia + System.lineSeparator();
            }
            
            ret.setContingut(str);
            
            ret.setData(new Date(System.currentTimeMillis()).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
            
            logicLayer.insertPost(ret);
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        
        return ret;
             
    }
    
    /**
     * Permet veure missatges
     */
    public void menuVeure()
    {
        System.out.println("LLISTAR MISSATGES");
        System.out.println("======= =========");
                        
        try 
        {
            LocalDateTime[] d = getDates();
            
            logicLayer.getMissatges(d).forEach(x -> System.out.println(x));
            
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    
     /***
     * Permet seleccionar un post i valorar-lo del 0 al 5
     */
    public void menuValorarPost()
    {
        
        System.out.println("VALORAR MISSATGE");
        System.out.println("======= ========");
                        
        try {
          
            LocalDateTime[] d = getDates();
            List<Post> mList = logicLayer.getMissatges(d);
            
            Iterator it = mList.iterator();
            
            boolean sortir = false;
            
            while (it.hasNext() && !sortir)
            {
                Post p = (Post)it.next();
                System.out.println(p);
                Scanner in = new Scanner(System.in);
                System.out.println("Afegir valoració? (s/n)");
                String v = in.nextLine();
                if (!v.toLowerCase().equals("s") && !v.toLowerCase().equals("n"))
                    throw new Exception ("Valor incorrecte");
                else if (v.toLowerCase().equals("s"))
                {
                    System.out.println("Valoració de 0 a 5?");
                    v = in.nextLine();
                    Integer val = Integer.parseInt(v);
                    if (val <0 || val>5)
                        throw new Exception ("Valor fora de rang");
                    p.getValoracio().add(val);
                    
                    logicLayer.updatePost(p);
                    
                    sortir = true;
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    
    /***
     * Elimina un post
     */
    public void menuEliminarPost()
    {
        try {
            
            System.out.println("ELIMINAR MISSATGE");
            System.out.println("======== ========");
                     
            LocalDateTime[] d = getDates();
            List<Post> mList = logicLayer.getMissatges(d);
            
            Iterator it = mList.iterator();
            
            boolean sortir = false;
            
            while (it.hasNext() || !sortir)
            {
                Post p = (Post)it.next();
                System.out.println(p);
                Scanner in = new Scanner(System.in);
                System.out.println("Eliminar? (s/n)");
                String v = in.nextLine();
                if (!v.toLowerCase().equals("s") && !v.toLowerCase().equals("n"))
                    throw new Exception ("Valor incorrecte");
                else if (v.toLowerCase().equals("s"))
                {
                    logicLayer.deletePost(p);
                    
                    sortir = true;
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
    
     /***
     * Demana data inicial i final a l'usuari
     * @return 
     */
    public static LocalDateTime[] getDates() throws Exception
    {
        LocalDateTime[] ret = new LocalDateTime[]{null,null};
        LocalDateTime def_ini, def_fi;
        String tmp;
        
        //dates per defecte si l'usuari prem intro
        def_ini = LocalDateTime.now().minusMinutes(60);
        def_fi = LocalDateTime.now().plusMinutes(60);
        
        
        Scanner console = new Scanner(System.in);
        System.out.println("Selecciona data i hora inicial ["+def_ini.format(Utils.getDataFormatter())+"]:");
        tmp = console.nextLine();
        if (tmp.isEmpty())
            ret[0]= def_ini;
        else
            ret[0] = LocalDateTime.parse(tmp,Utils.getDataFormatter());

        System.out.println("Selecciona data i hora final ["+def_fi.format(Utils.getDataFormatter())+"]):");
        tmp = console.nextLine();
        if (tmp.isEmpty())
            ret[1]= def_fi;
        else
            ret[1] = LocalDateTime.parse(tmp,Utils.getDataFormatter());

        if (ret[1].isBefore(ret[0]))
            throw new Exception ("Data final incorrecta: anterior a data inicial");
        
        
        return ret;
    }
    
    
    private static void printSeparador()
    {
         System.out.println("===================================================");
    }
    
}
