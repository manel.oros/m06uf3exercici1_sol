/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.entitats;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * Classe d'entitat que modela un Post (missatge)
 * 
 * @author manel
 */
public class Post {

   
    
    //identificador unic de mongoDB
    String idMongo;
    LocalDateTime data;
    String autor;
    String titol;
    String contingut;
    List<Integer> valoracio = new ArrayList<>();

    public Post() {
    }

    public List<Integer> getValoracio() {
        return valoracio;
    }

    public void setValoracio(List<Integer> valoracio) {
        this.valoracio = valoracio;
    }

    public LocalDateTime getData() {
        return data;
    } 

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getContingut() {
        return contingut;
    }

    public void setContingut(String contingut) {
        this.contingut = contingut;
    }
    
    public String getIdMongo() {
        return idMongo;
    }

    public void setIdMongo(String idMongo) {
        this.idMongo = idMongo;
    }
    
    /**
     * Retorna la mitjana de les valoracions del post
     * @return 
     */
    public Double getValoracioMitja()
    {
        Double ret = 0.0;
        Integer i;
        
        for(i=0;i<valoracio.size();i++)
        {
            ret += valoracio.get(i);
        }
        
        ret = ret/i;
        
        return ret;
    }
    
    /* @Override
    public String toString() {
        return "data=" + data.format(Utils.getDataFormatter()) + ", autor=" + autor + ", titol=" + titol + ", contingut=" + contingut + ", valoracio mitja=" + this.getValoracioMitja() + valoracio;
    }*/

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("==========================").append(System.lineSeparator());
        sb.append(" _id: ").append(idMongo).append(System.lineSeparator());
        sb.append(", data: ").append(data).append(System.lineSeparator());
        sb.append(", autor: ").append(autor).append(System.lineSeparator());
        sb.append(", titol: ").append(titol).append(System.lineSeparator());
        sb.append(", contingut: ").append(contingut).append(System.lineSeparator());
        sb.append(", valoracions: ").append(valoracio).append(System.lineSeparator()); 
        sb.append(", valoració mitja: ").append(this.getValoracioMitja()).append(System.lineSeparator());
        sb.append("==========================").append(System.lineSeparator());
        return sb.toString();
    }
}
