/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dades.entitats;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 * Capa de dades per a la classe Post
 * 
 * Realitza les operacions CRUD i la connexió a MongoDB
 * @author manel
 */
public class PostData {
    
    MongoDatabase bbdd;
    MongoCollection<Document> coleccio;
    MongoClient mongoClient;
    
     /***
     * Estableix la BBDD 
     * @param bd
     * @param db 
     */
    public void setDB(String bd)
    {
        MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
        mongoClient = new MongoClient(connectionString);
        bbdd = mongoClient.getDatabase(bd);
        this.setColeccio("col1");  
    }
    
    /***
     * Constructor
     * @param bd 
     */
    public PostData(String bd)
    {
        this.setDB(bd);
    }
    
    /***
     * Estableix la col.lecció d'accès
     * @param col 
     */
    private void setColeccio(String col)
    {
        coleccio = bbdd.getCollection(col);
    }
    
    /***
     * Insereix un document a la col.lecció
     * @param d 
     */
    public void insertOne(Document d)
    {
         coleccio.insertOne(d);
    }
    
    /**
     * Actualitza l'array de les valoracions del document
     * 
     * @param id identificador únic del document
     * @param d document amb les dades a actualitzar
     * @throws Exception 
     */
    public void updateOne(String id, Document d)  throws Exception
    {
        // Alhora de modificar un element de l'array, es pot fer de diverses maneres:
        // Afegint un element al vector del document
        // Substituint tot el vector del document per un amb els valors afegits
        // Substituint tot el document sencer
        // En aquest cas amb la comanda $ser substutium tot l'array pel nou array amb l'element afegit
        UpdateResult resultat = coleccio.updateOne(new Document("_id", new ObjectId(id)), new Document("$set", new Document("valoracio", d.get("valoracio") ))); 
       
        if (resultat.getModifiedCount() == 0)
            throw new Exception ("El document a actualitzar ja no existeix a la col.lecció");
    }
    
    /***
     * Elimina un document
     * @param id identificador del document a eliminar
     * @throws Exception 
     */
    public void deleteOne(String id) throws Exception
    {
         DeleteResult deleteResult = coleccio.deleteOne(new Document("_id", new ObjectId(id)));
         if (deleteResult.getDeletedCount() == 0)
            throw new Exception ("El document a eliminar ja no existeix a la col.lecció");
    }
    
    /***
     * 
     * @return 
     */
    public MongoClient getMongoClient() {
        return mongoClient;
    }
    
    /***
     * 
     * @return 
     */
    public MongoCollection<Document> getColeccio() {
        return coleccio;
    }
}
