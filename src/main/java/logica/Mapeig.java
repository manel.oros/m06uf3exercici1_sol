/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import dades.entitats.Post;
import org.bson.Document;

/**
 *
 * @author manel
 * 
 */
public class Mapeig {
    
    /***
     * Passa una classe Post a document
     * NO mapeja el _id
     * @param e
     * @return 
     */
    public static Document getDocument(Post e)
    {
        Document ret = new Document("data", e.getData())
                .append("autor", e.getAutor())
                .append("titol", e.getTitol())
                .append("contingut", e.getContingut())
                .append("valoracio", e.getValoracio());
        
        return ret;
    }
    
    /***
     * Passa un document a classe Post
     * Mapeja el _id
     * @param d
     * @return 
     */
    public static Post getPost(Document d)
    {
        Post ret = new Post();
        Document doc = new Document();
        
        //llegim les dades de les valoracions
        if ((List<Integer>)d.get("valoracio") != null)
            ret.setValoracio((List<Integer>)d.get("valoracio"));
        
        //llegim la resta de dades
        Date data = d.getDate("data");
        ret.setData(data.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        ret.setAutor(d.getString("autor"));
        ret.setTitol(d.getString("titol"));
        ret.setContingut(d.getString("contingut"));
        ret.setIdMongo(d.getObjectId("_id").toString());
        
        return ret;
    }
    
}
