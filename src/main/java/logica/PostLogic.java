/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lte;
import dades.entitats.PostData;
import dades.entitats.Post;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;

/**
 *
 * @author manel
 */
public class PostLogic {
    
    PostData dataLayer;

    public PostLogic(PostData d) {
        
        this.dataLayer = d;
    }
    
    /***
     * Retorna zero o més elements en funció de les dates inicial i final 
     * @param d (dates inicial i final)
     * @return 
     */
    public List<Post> getMissatges(LocalDateTime[] d)
    {
        List<Post> ret = new ArrayList<>();
        
          MongoCursor<Document> cursor = dataLayer.getColeccio().find(and(gte("data", d[0]),lte("data",d[1]))).iterator();

            try {
                while (cursor.hasNext()) {
                    ret.add(Mapeig.getPost(cursor.next()));
                }
            } finally {
                cursor.close();
            }
        
        return ret;
    }
    
    public void insertPost(Post p)
    {
        dataLayer.insertOne(Mapeig.getDocument(p));
    }
    
    public void updatePost(Post p) throws Exception
    {
       dataLayer.updateOne(p.getIdMongo(), Mapeig.getDocument(p));
    }
    
    public void deletePost(Post p) throws Exception
    {
       dataLayer.deleteOne(p.getIdMongo());   
    }
}
